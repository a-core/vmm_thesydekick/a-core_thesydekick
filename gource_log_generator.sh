#!/usr/bin/env bash

THISREPO="$( basename $(pwd))"
REPOS="\
    Entities/ACoreChip
    Entities/ACoreTestbenches
    Entities/ACoreTests
    Entities/chisel_methods
    Entities/inverter
    Entities/inv_sim
    Entities/JTAG
    Entities/jtag_programmer
    Entities/myentity
    Entities/register_template
    Entities/rtl
    Entities/spice
    Entities/thesdk
    Entities/thesdk_helpers
    "

rm -f ./gource_combined_tmp.txt
rm -f ./gource_combined_log.txt

gource --output-custom-log ./gource_combined_tmp.txt --max-files 0 ./
for repo in ${REPOS}; do
    echo $repo
    repologname="$(echo $repo | sed 's#/#_#g')"
    gource --output-custom-log ./${repologname}_log.txt --max-files 0 ./${repo}
    #sed -i -r "s#(.+)\|#\1|/${repo}#" ${repologname}_log.txt
    cat ${repologname}_log.txt >> ./gource_combined_tmp.txt
    rm -f  ${repologname}_log.txt 
done

sed -i -r "s#(.+)\|#\1|/"${THISREPO}"#" ./gource_combined_tmp.txt
sort -n ./gource_combined_tmp.txt > ./gource_combined_log.txt
rm -f ./gource_combined_tmp.txt

#gource -s 0.2 gource_combined_log.txt -hide usernames -start-date 2021-02-17 -stop-date 2022-10-25 -stop-at-end -output-framerate 25 
gource -s 0.2 gource_combined_log.txt -start-date 2021-02-17 -stop-at-end -output-framerate 25 

exit 0



